#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include "quicksort/qicksort.h"
#include "mergesort/mergesort.h"

using namespace std;

void show(const vector<int> &vec){
    vector<int>::const_iterator iter;
    int cols = 9;
    int col = 0;
    for(iter = vec.begin(); iter != vec.end(); iter++){
        if(col < cols){
            cout<<*iter<<"\t";
            col++;
        }else{
            cout<<*iter<<"\n";
            col = 0;
        }
    }
}

int main()
{
    vector<int> vInt;
    clock_t start;

    int n = 100;
    for(int i = n-1; i>=0; i--){
        vInt.push_back(rand()%1000);
    }
    show(vInt);

    cout<<"****** quicksort *******\n";
    vector<int> vInt1 = vInt;
    start = clock();
    quicksort(vInt1, 0, vInt1.size()-1);
    cout<<"time: "<<(double)(clock()-start)/CLOCKS_PER_SEC<<";\n";
    show(vInt1);

    cout<<"****** rundomized_quicksort *******\n";
    vector<int> vInt2 = vInt;
    start = clock();
    rundomized_quicksort(vInt2, 0, vInt2.size()-1);
    cout<<"time: "<<(double)(clock()-start)/CLOCKS_PER_SEC<<";\n";
    show(vInt2);

    cout<<"******* mergesort ******\n";
    vector<int> vInt3 = vInt;
    start = clock();
    mergesort(vInt3, 0, vInt3.size()-1);
    cout<<"time: "<<(double)(clock()-start)/CLOCKS_PER_SEC<<";\n";
    show(vInt3);

    return 0;
}
