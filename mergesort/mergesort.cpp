#include "mergesort.h"

void merge(std::vector<int> &vec, int begin, int q, int end){
    std::vector<int> tmp;
    int i = begin;
    int j = q+1;

    while(true){
        if(vec[i] <= vec[j]){
            tmp.push_back(vec[i]);
            i++;
        }else{
            tmp.push_back(vec[j]);
            j++;
        }

        if(i > q || j > end) break;
    }

    if(i<=q) for(; i<=q; i++) tmp.push_back(vec[i]);

    if(j<=end) for (; j<=end; j++) tmp.push_back(vec[j]);

    for(i = 0; i <= end-begin; i++){
        vec[begin+i] = tmp[i];
    }
}

void mergesort(std::vector<int> &vec, int begin, int end){
    if(begin < end) {
        int q = (int) (end - begin)/2 + begin;
        mergesort(vec, begin, q);
        mergesort(vec, q+1, end);
        merge(vec, begin, q, end);
    }
}
