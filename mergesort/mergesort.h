#include <vector>

#ifndef MERGESORT_INCLUDED
#define MERGESORT_INCLUDED

void mergesort(std::vector<int> &vec, int begin, int end);

#endif // MERGESORT_INCLUDED
