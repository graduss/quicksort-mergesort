#include <vector>
#include <cstdlib>

#ifndef QICKSORT_INCLUDED
#define QICKSORT_INCLUDED

void quicksort(std::vector<int> &vec, int begin, int end);

void rundomized_quicksort(std::vector<int> &vec, int begin, int end);

#endif // QICKSORT_INCLUDED
