#include "qicksort.h"

int partition(std::vector<int> &vec, int begin, int end){
    int x = vec[begin];
    int i = begin - 1;
    int j = end + 1;
    int temp;

    while(true){
        do i++;
        while (vec[i]<x);

        do j--;
        while (vec[j]>x);

        if(i<j) {
            temp = vec[i];
            vec[i] = vec[j];
            vec[j] = temp;
        }else return j;
    }
}

int random(int i, int j){
    int k = (std::rand()%1000)/1000;
    return (i-j)*k + i;
}

int rundomized_partition(std::vector<int> &vec, int begin, int end){
    int i = random(begin, end);
    int tmp = vec[begin];
    vec[begin] = vec[i];
    vec[i] = tmp;

    return partition(vec, begin, end);
}

int last_element(std::vector<int> &vec, int begin, int end){
    int tmp = vec[begin];
    vec[begin] = vec[end];
    vec[end] = tmp;

    return partition(vec, begin, end);
}

int center_element(std::vector<int> &vec, int begin, int end){
    int i = (int) begin + (end - begin)/2;
    int tmp = vec[begin];
    vec[begin] = vec[i];
    vec[i] = tmp;

    return partition(vec, begin, end);
}


void quicksort(std::vector<int> &vec, int begin, int end){
    if(begin < end) {
        //int q = partition(vec, begin, end);         // first
        //int q = last_element(vec, begin, end);      // last
        int q = center_element(vec, begin, end);    // center
        quicksort(vec, begin, q);
        quicksort(vec, q+1, end);
    }
}

void rundomized_quicksort(std::vector<int> &vec, int begin, int end){
    if(begin < end){
        int q = rundomized_partition(vec, begin, end);
        rundomized_quicksort(vec, begin, q);
        rundomized_quicksort(vec, q+1, end);
    }
}
